locals {
  db_username = "c3-vault"
}

terraform {
  backend "consul" {
    path = "terraform/database-sql-postgresql"
  }

  required_providers {
    random = {
      source = "hashicorp/random"
      version = "3.6.0"
    }

    nomad = {
      source = "hashicorp/nomad"
      version = "2.1.0"
    }

    vault = {
      source = "hashicorp/vault"
      version = "3.23.0"
    }
  }
}

provider "random" {}
provider "nomad" {}
provider "vault" {}

// Initial Root Password (Vault will take over and change this on first run)

resource "random_password" "postgresql_init_password" {
  length           = 24
  special          = true
  override_special = "!$*()-_+[]{}<>:"
}

resource "vault_kv_secret_v2" "example" {
  mount               = "c3kv"
  name                = "datacenter/soc/database/postgresql"
  cas                 = 1
  delete_all_versions = true

  data_json = jsonencode({
    initRootPassword = "${random_password.postgresql_init_password.result}",
    initRootUsername = "${local.db_username}"
  })
}

resource "vault_mount" "db" {
  path = "c3-postgresql"
  type = "database"
}

resource "vault_database_secret_backend_connection" "postgres" {
  backend       = vault_mount.db.path
  name          = "postgres"
  allowed_roles = []

  postgresql {
    connection_url = "postgres://{{username}}:{{password}}@postgres.soc.carboncollins.se:5432/database"

    username = local.db_username
    password = random_password.postgresql_init_password.result
  }
}
