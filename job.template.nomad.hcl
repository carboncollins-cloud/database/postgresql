job "database-postgresql" {
  name        = "Database SQL (PostgreSQL)"
  type        = "service"
  region      = "se"
  datacenters = ["soc"]
  namespace   = "c3-database"

  priority = 60

  group "postgresql" {
    count = 1

    consul {}

    network {
      mode = "bridge"
    }

    volume "postgresql-data" {
      type      = "host"
      read_only = false
      source    = "postgresql-data"
    }

    restart {
      attempts = 10
      interval = "5m"
      delay    = "25s"
      mode     = "delay"
    }

    service {
      provider = "consul"
      name     = "postgresql"
      port     = "5432"
      task     = "postgresql"

      connect {
        sidecar_service {}
      }

      tags = [
        "internal-proxy.enable=true",
        "internal-proxy.consulcatalog.connect=true",
        "internal-proxy.tcp.routers.postgresql.rule=HostSNI(`*`)",
        "internal-proxy.tcp.routers.postgresql.entryPoints=postgresql"
      ]
    }

    task "postgresql" {  
      driver = "docker"
      leader = true

      volume_mount {
        volume      = "postgresql-data"
        destination = "/var/lib/postgresql/data"
        read_only   = false
      }

      vault {
        role = "service-postgresql"
      }

      resources {
        cpu    = 1000
        memory = 1024
      }

      config {
        image = "postgres:16-alpine"
      }

      env {
        TZ          = "Europe/Stockholm"
        POSTGRES_DB = "default-database"
        PGDATA      = "/var/lib/postgresql/data/pgdata"
      }

      template {
        data = <<EOH
        {{ with secret "c3kv/data/datacenter/soc/database/postgresql" }}
          POSTGRES_PASSWORD={{ index .Data.data "initRootPassword" }}
          POSTGRES_USER={{ index .Data.data "initRootUsername" }}
        {{ end }}
        EOH

        destination = "secrets/postgresql.env"
        env = true
      }
    }
  }

  reschedule {
    delay          = "10s"
    delay_function = "exponential"
    max_delay      = "10m"
    unlimited      = true
  }

  update {
    health_check      = "checks"
    min_healthy_time  = "10s"
    healthy_deadline  = "10m"
    progress_deadline = "15m"
    auto_revert       = true
  }

  meta {
    gitSha      = "[[ .gitSha ]]"
    gitBranch   = "[[ .gitBranch ]]"
    pipelineId  = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId   = "[[ .projectId ]]"
    projectUrl  = "[[ .projectUrl ]]"
    statefull   = "true"
  }
}
